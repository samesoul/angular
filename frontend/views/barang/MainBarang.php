<?php
namespace app\views\barang;
/**
* 
*/

class MainBarang
{

    public $widget = [
    'requires' => ['ngResource','ui.bootstrap','dee.ui'],
    'routes' => [
        '/barang/' => [
            'title' => 'Barang',
            'view' => '/barang/index',
            'activetab' => 'barang',
            'js' => '/barang/js/index.js',
            'injection' => ['Barang','Barang1'],
        ],
        '/barang/create' => [
            'title' => 'Create',
            'view' => '/barang/create',
            'activetab' => 'barang',
            'js' => '/barang/js/create.js',
            'injection' => ['BarangCreate',],
        ],
        '/barang/:id/edit' => [
            'title' => 'Update',
            'view' => '/barang/update',
            'activetab' => 'barang',
            'js' => '/barang/js/update.js',
            'injection' => ['BarangEdit',],
        ],
        '/barang/:id' => [
            'title' => 'View',
            'view' => '/barang/view',
            'activetab' => 'barang',
            'js' => '/barang/js/view.js',
            'injection' => ['Barang1',],
        ],
    ],
    'resources' => [
        'Barang' => [
        'url' => '/group/frontend/web/barang/tampil',
            'actions' =>[
                'get' => [
                    'method'=>'GET',
                    'isArray'=> true,
                ],
                'update' => [
                    'method'=>'PUT',
                ],
            ]
        ],
        'Barang1' => [
        'url' => '/group/frontend/web/barang/view?id=:id',
            'actions' =>[
                'get' => [
                    'method' => 'GET',
                    'isArray'=> false,
                ],
            ]
        ],
        'Barang2' => [
        'url' => '/group/frontend/web/barang/tampil',
            'actions' =>[
                'get' => [
                    'method'=>'GET',
                    'isArray'=> true,
                ],
            ]
        ],
        'BarangCreate' => [
        //'headers'=> ['Content-Type'=> 'application/x-www-form-urlencoded'],
        'url' => '/group/frontend/web/barang/create',
        'Content-Type' => 'application/json',
        /*
        'actions' =>[
                'get' => [
                    'method'=>'POST',
                    'isArray'=> true,
                ],
            ]
        */
        ],
        'BarangEdit' => [
        'url' => '/group/frontend/web/barang/update?id=:id',
            'actions' =>[
                'get' => [
                    'method' => 'GET',
                    'isArray'=> false,
                ],
            ]
        ],
    ], 
];
}
?>
