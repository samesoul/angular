<?php
use dee\angular\NgView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $widget NgView */
?>

<div class="barang-view">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <a ng-href="#/barang/{{paramId}}/edit" class="btn btn-primary">Update</a>
        <a href ng-click="deleteModel()"class="btn btn-danger">Delete</a>
    </p>

    <table class="table table-striped table-bordered detail-view">
        <tr><th>ID</th><td>{{model.id}}</td></tr>
        <tr><th>Nama</th><td>{{model.nama}}</td></tr>
        <tr><th>Jenis Barang</th><td>{{model.jenis_barang}}</td></tr>
    </table>
</div>