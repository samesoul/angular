<?php
use dee\angular\NgView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $widget NgView */
//$title='aaa';
use kartik\growl\Growl;

?>
<div id='messages'></div>
<div  ng-show='hasil' class="alert-danger alert fade in" id="message" style="opacity: 293.519;">

<i class="icon fa fa-warning"></i>{{hasil}}

</div>

<div class="barang-index">
    <h1><?= Html::encode($this->title.' - Barang') ?></h1>
    <p>
        <?= Html::a('Create', '#/barang/create', ['class' => 'btn btn-success']) ?>    </p>
    <div class="pull-right">
        Show 
        <select name="" id="" ng-model="provider.itemPerPage" ng-options="p.showPage as p.showPage for p in sizePaging">
            
        </select> 
        <label for="w1">items</label> 
    </div>
    <div class="grid-view">
    <div class="summary">Showing <b>1-{{numPerPage}}</b> of <b>{{rows.length}}</b> items.</div>

        <table class="table table-striped table-bordered" >
            <thead>
                <tr d-sort ng-model="provider.sortType" ng-change="sortReverse = !sortReverse" multisort="false">
                    <th>#</th>
                    <th><a href sort-field="id">Id</a></th>
                    <th><a href sort-field="nama">Nama</a></th>
                    <th><a href sort-field="jenis_barang">Jenis_barang</a></th>
                    <th></th>
                </tr>
                <tr class="filters" id="w3-filters">
                    <td>&nbsp;</td>
                    <td><input type="text" ng-model="q.id" placeholder='Search Id' ng-change="filter()" class="form-control"></td>
                    <td><input type="text" ng-model="q.nama" placeholder='Search Nama' ng-change="filter()" class="form-control"></td>
                    <td><input type="text" ng-model="q.jenis_barang" placeholder="Search" ng-change="filter()" class="form-control"></td>
                    <td>&nbsp;</td>
                </tr>
            </thead>
            <tbody>
                <tr ng-repeat="model in filtered.slice(provider.offset, provider.offset + provider.itemPerPage) | orderBy:sortType :sortReverse">
                    <td>{{provider.offset + $index + 1}}</td>
                    <td>{{model.id}}</td>
                    <td>{{model.nama}}</td>
                    <td>{{model.jenis_barang}}</td>
                    <td>
                        <a ng-href="#/barang/{{model.id}}"><span class="glyphicon glyphicon-eye-open"></span></a>
                        <a ng-href="#/barang/{{model.id}}/edit"><span class="glyphicon glyphicon-pencil"></span></a>
                        <a href ng-click="deleteModel(model)"><span class="glyphicon glyphicon-trash"></span></a>
                    </td>
                </tr>
            </tbody>
        </table>
            <pagination total-items="filtered.length" ng-model="provider.page"
                        max-size="5" items-per-page="provider.itemPerPage"
                        ng-change="provider.paging()" direction-links="false"
                        first-text="&laquo;" last-text="&raquo;"
                        class="pagination-sm" boundary-links="true">
            </pagination>
    </div>
</div>
