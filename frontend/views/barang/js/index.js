var $location = $injector.get('$location');
var search = $location.search();
var $filter = $injector.get('$filter');
var $modal = $injector.get('$modal');
var $timeout = $injector.get('$timeout');

$scope.rows = [];
$scope.q = '';

query = function () {
    Barang.query({
        //expand:'parentName,menuParent'
    }, function (rows) {
        $scope.rows = rows;
        $scope.filter();
    });
}
query();

//sorting
$scope.sizePaging =[
        {showPage: 5},
        {showPage: 10},
        {showPage: 30},
        {showPage: 50}
    ]; 
    
// data provider
$scope.provider = {
    offset: 0,
    page: 1,
    itemPerPage: 10,
    paging: function () {
        $scope.provider.offset = ($scope.provider.page - 1) * $scope.provider.itemPerPage;
    },
    sortType  : 'nama',
    sortReverse  : false
};

$scope.filter = function () {
    $scope.filtered = $filter('filter')($scope.rows, $scope.q);
}

$scope.deleteItem = function (item) {
    if (confirm('Are you sure you want to delete?')) {
        Menu.remove({id: item.id}, {}, function () {
            addAlert('info', 'Menu deleted');
            query();
        }, function (r) {
            addAlert('error', r.statusText);
        });
    }
}
$scope.deleteModel = function(model){
    if(confirm('Are you sure you want to delete')){
        id = model.id;
        Barang1.remove({id:id},{},function(){
            query();
            $scope.hasil = 'id : '+id+' telah terhapus';
            $('#messages').append('<div data-notify="container" role="alert" class="col-xs-11 col-sm-3 alert alert-success animated fadeInDown" id="w0" data-notify-position="top-right" style="display: inline-block; margin: 0px auto; position: fixed; transition: all 0.5s ease-in-out 0s; z-index: 1031; top: 20px; right: 20px;"><button data-notify="dismiss" class="close" type="button"><span aria-hidden="true">×</span></button><span data-notify="icon" class="glyphicon glyphicon-ok-sign"></span><span data-notify="title">Note</span><hr class="kv-alert-separator"><span data-notify="message">This is a successful growling alert.</span><a target="_blank" data-notify="url" href="#"></a></div>');
            $('#message').slideDown(500);
            $timeout(callAtInterval, 5000);
            function callAtInterval() {
            $scope.hasil = '';
            $('#message').fadeIn(500);
            }
        });
    }
}



$scope.alerts = [];
addAlert = function (type, msg) {
    var alert = {type: type, msg: msg};
    if (type == 'info') {
        alert.timeout = 3000;
    }
    $scope.alerts.push(alert);
};

$scope.closeAlert = function (index) {
    $scope.alerts.splice(index, 1);
};