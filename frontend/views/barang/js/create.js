
$location = $injector.get('$location');
// model
$scope.model = {};


//alert($location.path());
// save Item
$scope.save = function(){
    BarangCreate.save({},$scope.model,function(model){
        id = model.id;
        $location.path('/barang/' + id);
        //console.log(model);
    },function(r){
        //console.log(r);
        $scope.errors = {};
        $scope.errorStatus = r.status;
        $scope.errorText = r.statusTest;
        if (r.status == 422) {
            angular.forEach(r.data,function(err) {
                $scope.errors[err.field] = err.message;
            });
        }
    });
}

$scope.discard = function(){
    window.history.back();
}
