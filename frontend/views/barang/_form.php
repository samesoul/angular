<?php
use dee\angular\NgView;
use yii\base\Widget as WebView1;

/* @var $this yii\web\View */
/* @var $widget NgView */

//$widget->renderJs('js/form.js');

//$view1 = $this->WebView1();
        //print_r($view);

?>

<div class="barang-form">
    <form name="Form" >
        <div ng-if="errorStatus">
            <h1>Error {{errorStatus}}: {{errorText}}</h1>
            <ul>
                <li ng-repeat="(field,msg) in errors">{{field}}: {{msg}}</li>
            </ul>
        </div>
        <!--
        <div class="form-group" ng-class="{error:!!errors.id}">
            <label for="barang-nama" class="control-label">id</label>
            <input id="barang-id" name="id" class="form-control" ng-model="model.id">
            <div class="help-block" ng-bind"errors.id"></div>
        </div>
        -->
        <div class="form-group" ng-class="{error:!!errors.nama}">
            <label for="barang-nama" class="control-label">Nama</label>
            <input id="barang-nama" name="nama" class="form-control" ng-model="model.nama">
            <div class="help-block" ng-bind"errors.nama"></div>
        </div>
        <div class="form-group" ng-class="{error:!!errors.jenis_barang}">
            <label for="barang-jenis_barang" class="control-label">Jenis Barang</label>
            <input id="barang-jenis_barang" name="jenis_barang" class="form-control" ng-model="model.jenis_barang">
            <div class="help-block" ng-bind"errors.jenis_barang"></div>
        </div>
        <div class="form-group">
            <button class="btn btn-primary" ng-click="save()">Save</button>
            <button class="btn btn-danger" ng-click="discard()">Back</button>
        </div>
    </form>
</div>