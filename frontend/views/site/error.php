<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

//$this->title = $name;

//$this->title = "Not Found (#404)";
?>
<div class="site-error">

    <h1>PAGE NOT FOUND</h1>

    <div class="alert alert-danger">
        <?= nl2br(Html::encode('Page not found.')) ?>
    </div>

    <p>
        The above error occurred while the Web server was processing your request.
    </p>
    <p>
        Please contact us if you think this is a server error. Thank you.
    </p>

</div>