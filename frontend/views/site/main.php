<?php
use app\views\barang\MainBarang;

use yii\helpers\Url;
use dee\angular\NgView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
$this->title = 'My Yii Application';
$MainBarang = new MainBarang();
$routes = $MainBarang->widget['routes'];
$resources = $MainBarang->widget['resources'];
$indexroutes = [
        '/index' => [
            'view' => 'index',
            'title' => 'Yii My Application',
            'activetab' => 'home',
        ],
        '/login' => [
            'view' => 'login',
            'title' => 'Yii My Application',
            'activetab' => 'login',
            'js' => '/site/js/create.js',
            'injection' => ['login',],
        ],
        'otherwise' => [
            'title' => 'Not Found (#404)',
            'view' => 'error',
        ],
    ];
$indexresources = [
        'login' => [
        'url' => '/group/frontend/web/site/login',
            'actions' =>[
                'get' => [
                    'method'=>'GET',
                    'isArray'=> true,
                ],
                'update' => [
                    'method'=>'PUT',
                ],
            ]
        ],
    ];
$routes = ArrayHelper::merge( $routes   , $indexroutes );
$resources = ArrayHelper::merge( $resources   , $indexresources );
?>

<?=
NgView::widget([
    'useNgApp' => false,
    'js' => 'js/index.js',
    'requires' => ['ngResource','ui.bootstrap','dee.ui'],
    'routes' => $routes,
    'resources' => $resources, 
]);
?>
