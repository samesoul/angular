<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Login';
//$this->params['breadcrumbs'][] = $this->title;
?>
<h1>Login</h1>

<p>Please fill out the following fields to login:</p>

<div class="row">
    <div class="col-lg-5">
        <form  name="loginForm">
            <div ng-class="{ 'has-success': !error['username'] && submitted,
                'has-error': error['username'] && submitted }"
                 class="form-group field-loginform-username required">
                <label class="control-label" for="loginform-username">Username(demo)</label>
                <input ng-model="userModel.username" type="text" id="loginform-username" class="form-control">
                <p class="help-block help-block-error">{{ error['username'] }}</p>
            </div>

            <div ng-class="{ 'has-success': !error['password'] && submitted,
                'has-error': error['password'] && submitted }"
                 class="form-group field-loginform-password required">
                <label class="control-label" for="loginform-password">Password(demodemo)</label>
                <input ng-model="userModel.password" type="password" id="loginform-password" class="form-control">
                <p class="help-block help-block-error">{{ error['password'] }}</p>
            </div>

            <div class="form-group">
                <button class="btn btn-primary" ng-click="login()">Login</button>
            </div>

        </form>
    </div>
</div>

