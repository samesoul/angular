
module.run(['$rootScope', '$route',
    function ($rootScope, $route) {
        $rootScope.$on('$routeChangeSuccess', function() {
        document.title = $route.current.title;
    });
}]);

module.controller('AppCtrl', ['$scope','$route', '$location', '$window', function ($scope, $route, $location, $window ) {         
        $scope.$route = $route;

        $scope.loggedIn = function() {
            //console.log($window);
            return Boolean($window.sessionStorage.access_token);
        };

        $scope.logout = function () {
            //console.log($window);
            delete $window.sessionStorage.access_token;
            $location.path('/login').replace();
        };
}]);

module.config(['$httpProvider',function($httpProvider){
$httpProvider.interceptors.push('authInterceptor');
}]);

module.factory('authInterceptor', function ($q, $window, $location) {
    return {
        request: function (config) {
            if ($window.sessionStorage.access_token) {
                //HttpBearerAuth
                //console.log(config);
                config.headers.Authorization = 'Bearer ' + $window.sessionStorage.access_token;
            }
            return config;
        },
        responseError: function (rejection) {
            if (rejection.status === 401) {
                $location.path('/login').replace();
            }
            return $q.reject(rejection);
        }
    };
});

