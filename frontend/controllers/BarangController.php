<?php

namespace frontend\controllers;

use Yii;
use app\models\Barang;
use app\models\BarangSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\AccessControl;

/**
 * BarangController implements the CRUD actions for Barang model.
 */
class BarangController extends Controller
{   
    //for ngolehno nangkep post 
    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }
    /**
     * Lists all Barang models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BarangSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('main', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionTampil()
    {
        $searchModel = new BarangSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $sql = 'SELECT * from barang order by id desc';
                $Siswa2 = $searchModel::findBySql($sql)->all();

        return JSon::encode($Siswa2);
    }

    /**
     * Displays a single Barang model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $request    = Yii::$app->request;
        $model      = $this->findModel($id);

        if ($params = $request->isDelete) {
            $model->delete();
        } else {
            $model  = $model->attributes;
            return JSon::encode($model);
        }
    }

    /**
     * Creates a new Barang model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {

        $post = file_get_contents("php://input");

        $data = JSon::decode($post, true);
        $model = new Barang();
        $model->attributes = $data;
        if ($model->save() === false) {
            $model = $model->errors;
        } else {
            $model = $model; 
        }

        header('Content-type:application/json, text/plain, */*');
  
        echo JSon::encode($model);
        exit();
    }

    /**
     * Updates an existing Barang model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $post = file_get_contents("php://input");
        if ($post) {
            $data = JSon::decode($post, true);
            $model->attributes = $data;
            if ($model->save() === false) {
                $model = $model->errors;
            } else {
                $model = $model; 
            }
            header('Content-type:application/json, text/plain, */*');
            echo JSon::encode($model);
            exit();
        } else {
            $model = $model->attributes;
            return JSon::encode($model);
        }
    }

    /**
     * Deletes an existing Barang model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Barang model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Barang the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Barang::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
