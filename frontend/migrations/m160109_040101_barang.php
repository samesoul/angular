<?php

use yii\db\Schema;

class m160109_040101_barang extends \yii\db\Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        
        $this->createTable('barang', [
            'id' => $this->primaryKey(),
            'nama' => $this->string(255),
            'jenis_barang' => $this->string(255),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('barang');
    }
}
